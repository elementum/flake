{
  description = "init flake";

  outputs = inputs @ {
    self,
    nixpkgs,
    flake-utils,
    pre-commit-hooks,
    haumea,
    ...
  }: let
    systems = with flake-utils.lib.system; [
      x86_64-linux
    ];
    systemAttrs = flake-utils.lib.eachSystem systems (system: let
      inherit (lib.my) importNixFiles;
      pre-commit-check = pre-commit-hooks.lib.${system}.run {
        src = self.outPath;
        hooks = {
          alejandra.enable = true;
          deadnix.enable = true;
          statix.enable = true;
        };
      };

      nixpkgs-config = {
        inherit system overlays;
        config.allowUnfree = true;
      };

      pkgs = import nixpkgs nixpkgs-config;

      src = {
        # deadnix: skip
        default = {pkgs, ...} @ args:
          haumea.lib.load {
            src = ./src;
            inputs =
              args
              // {
                inherit inputs;
              };
            transformer = haumea.lib.transformers.liftDefault;
          };
      };

      overlays = with inputs;
        [
          emacs.overlay
          neovim-nightly-overlay.overlay
          rust-overlay.overlays.default
          hyprland.overlays.default
          hyprland-contrib.overlays.default
          eww.overlays.default
          zig.overlays.default
          # (
          #   final: _: let
          #     inherit (final) system;
          #   in {
          #     sf-mono-liga-src = sf-mono-liga;
          #     eww-wayland-git = eww.packages.${system}.eww-wayland;
          #   }
          # )
        ]
        ++ (importNixFiles ./overlays);

      lib = nixpkgs.lib.extend (final: prev: {
        my = import ./lib {
          inherit pkgs inputs nixpkgs;
          lib = final;
        };
      });
    in {
      lib = lib.my;
      checks = {inherit pre-commit-check;};
      nixosConfigurations = import ./hosts {
        inherit self pkgs nixpkgs system inputs src;
      };
      iso = import ./iso {
        inherit self nixpkgs inputs pkgs;
      };
    });
    system = "x86_64-linux";
  in
    systemAttrs
    // {
      nixosConfigurations = systemAttrs.nixosConfigurations."${system}";

      # TODO what is templates?
      templates = {
        full = {
          path = self.outPath;
          description = "A completely non-minimal NixOS configuration";
        };
      };
    };

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    grub2-themes.url = "github:vinceliuice/grub2-themes";
    emacs = {
      url = "github:nix-community/emacs-overlay";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
    haumea = {
      url = "github:nix-community/haumea";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    hyprland = {
      url = "github:hyprwm/Hyprland";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    hyprland-contrib = {
      url = "github:hyprwm/contrib";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    eww = {
      url = "github:elkowar/eww";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        rust-overlay.follows = "rust-overlay";
      };
    };
    helix = {
      url = "github:helix-editor/helix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.rust-overlay.follows = "rust-overlay";
    };
    neovim-nightly-overlay = {
      url = "github:nix-community/neovim-nightly-overlay";
    };
    xdg-portal-hyprland.url = "github:hyprwm/xdg-desktop-portal-hyprland";
    flake-utils.url = "github:numtide/flake-utils";
    zig.url = "github:mitchellh/zig-overlay";
    # nixpkgs-wayland.url = "github:nix-community/nixpkgs-wayland";
    sf-mono-liga = {
      url = "github:shaunsingh/SFMono-Nerd-Font-Ligaturized";
      flake = false;
    };
    wired.url = "github:Toqozz/wired-notify";
    # TODO enable
    musnix = {
      url = "github:musnix/musnix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs = {
        flake-utils.follows = "flake-utils";
        nixpkgs.follows = "nixpkgs";
        nixpkgs-stable.follows = "nixpkgs";
      };
    };
  };
}
