{nixpkgs, ...}: let
  # builtins.readDir, but recurses into directories
  readDirRecursive = dir:
    nixpkgs.lib.mapAttrs
    (name: type:
      if type == "directory"
      then readDirRecursive "${dir}/${name}"
      else type)
    (builtins.readDir dir); # adapatation of f2k's config that handles recursive reads
  filterNixFiles = k: v: v == "regular" && nixpkgs.lib.hasSuffix ".nix" k;
in {
  importNixFiles = path:
    with nixpkgs.lib;
      (lists.forEach (mapAttrsToList (name: _: path + ("/" + name))
          (filterAttrs filterNixFiles (readDirRecursive path))))
      import;
}
