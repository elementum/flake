# README

## inspiration

- [Icy-Thought](https://github.com/Icy-Thought/snowflake)
- [sioodmy](https://github.com/sioodmy/dotfiles)
- [shaunsingh](https://github.com/shaunsingh/nix-darwin-dotfiles)
- [luisholanda](https://github.com/luisholanda/dotfiles)
- [iliayar](https://github.com/iliayar/dotfiles)

## log

- cf1dcaa confirmed browser gpu acceleration vivaldi

## Structure

- home: mods for all hosts
- hosts: host specific mods

## TODO

- GNOME keyring
- implemement stable/unstable/master pkgs
- implement module system
- make iso
- implement disko
- default settings for hosts
- timeZone
