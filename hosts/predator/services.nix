{root}: {
  xserver.videoDrivers = ["nvidia"];
  # fstrim.enable = true; # for SSD
  pcscd.enable = true;
  getty.autologinUser = root.cfg.username;
  devmon.enable = true;
  printing.enable = true;
  logind = {
    lidSwitch = "suspend-then-hibernate";
    lidSwitchExternalPower = "lock";
    extraConfig = ''
      HandlePowerKey=suspend-then-hibernate
      HibernateDelaySec=3600
    '';
  }; # blueman.enable = true;
  openssh.enable = true;
  udev.extraRules = ''
    KERNEL=="hidraw*", SUBSYSTEM=="hidraw", MODE="0666", TAG+="uaccess", TAG+="udev-acl"

    SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="3748", MODE:="0666", SYMLINK+="stlinkv2_%n"
  '';
  pipewire = {
    enable = true;
    alsa = {
      enable = true;
      support32Bit = true;
    };
    wireplumber.enable = true;
    jack.enable = true;
    pulse.enable = true;
  };
}
