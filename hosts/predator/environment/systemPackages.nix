{root, ...}:
with root.cfg;
  builtins.concatLists [
    bloat
    nvidia
    lsp
    tools
    wayland
  ]
