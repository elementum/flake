{
  inputs,
  pkgs,
  lib,
  ...
}: let
  inherit (lib.meta) getExe;
in {
  time = "America/Los_Angeles";
  state = "23.05";
  system = "x86_64-linux";
  username = "nix";
  key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDCs1BIuADRyAD3dUP96KaLbMRhnja2+qt5KrwjQwl/Jyn/relfXvhtb0KXXrb7HbXWH3U91FN5PekavGQHIVM6oCO/M4/labKvj29kFLN5lVT6gN56QbaY0L2sD0g5x/WgUbX4eG60PdFIQjIpVOXBBX43VOCGPadCLqF1440j32sJ0K5A83GXqzdIvzSv8Pn57VgOehgtYtBpD3WREgOA+sGOWHKbUvavYa6dNW4LsQQt7hBfmln3NV6NCORvYLu1XKyzTtvQ6RxZ5mhvDklw14h81jD/ktpMXuMITKsbPl/I9OjXUtKQ5NbS0t57taZoQjGqiC05rW64MbQ0ZjayOCaiSEGJOIhkFH5eVs3ZmCoZAESlBUl4bMhyMz7w/aUWQlEfHp4BKXzHUYr7xyqvjIaG+jcTuUKoNGptLlmoJL2Vl52T6nFYHKd4p6eSq/njRGF56yjOczvWm/t3DvBW0+ki0HjzhVKnc4FdqhD/O1+kTt48IHIloNCgVXKKVnOlCzrOubOey14FcLSltv9eJNt46oUhe/cl1ay2RNZAKKFPct3iT7kAVBYpa3YwdPyDOUIniYLRru1vBvIuQzx5+DC9k5lgdKL+ZXMGfMJRj3WOVRKxV8ggUWsbOBP5MyxI9Xo9se3eqzN+HK0a1SMQ+p9d6QURwTuDbaxmgXA9KQ== main";
  pass = "$6$OFaW4HEqtPombFgP$fVK7rjzVVeQGSEkzqmcVsElZcfaO9q66iu.vmBrEUgiZ0ZuUci7NFh0SkI5wP09/b5KPKnad6BEGqZGugPPZp/";
  nvidia = with pkgs; [
    nvidia-vaapi-driver
    glxinfo # nvidia
    pciutils # lscpi
    acpi
  ];
  river = with pkgs; [
    way-displays
    seatd
    river
    river-tag-overlay
  ];
  wayland = with pkgs; [
    slurp
    wayshot
    inputs.wired.packages.${system}.wired
    xwayland
    waylock
    swayidle
    brightnessctl
    wev # wayland event viewer (debugging)
    eww-wayland
    # eww-wayland-git
    egl-wayland
    hyprpicker
    wlr-randr
    wl-clipboard
    obs-studio-plugins.wlrobs
  ];
  emulation = with pkgs; [
    cen64
    mupen64plus
    ryujinx
    yuzu-mainline
  ];
  tools = with pkgs; [
    fd
    coreutils
    ffmpeg
    psmisc # process misc tools
    mold # compiler linker
    sccache # compiler cache
    sd
    sshfs
    nnn
    rsync
    skim
    ripgrep
    lsd
    jc
    jq
    git
    neofetch
    freshfetch
    ueberzug
    wget
    curl
    exa
    socat
    python3
    fzf
    tealdeer
    cached-nix-shell
    gnumake
    unrar
    unzip
    zoxide
    starship
    delta
    emoji-picker
    mpc_cli
    nix-index # nix-locate bins
    nixpkgs-fmt
    ouch # compression
    pamixer
    wtype
    lls
    redshift
    libsixel
    tree
    vivid
    lf
    trashy
    atool # als?
    p7zip
    htop
    bottom
    libarchive
    glow
    hexyl
    bat
    chafa
    exiv2
    mediainfo
    odt2txt
    pandoc
    poppler_utils # view pdf
    xmlstarlet # epub
    sqlite
    emacsPackages.ansilove # ansi
    tty-clock
    lazygit
    aria
    openssl
  ];
  lsp = with pkgs; [
    # gef
    # gdb
    statix # nix linter
    lldb # debugger
    nodePackages.bash-language-server
    nodePackages.typescript-language-server # Typescript
    nodePackages.vscode-json-languageserver # JSON
    shfmt
    alejandra
    taplo
    emacs
    marksman
    ltex-ls
    tree-sitter
    sumneko-lua-language-server
    lua
    yarn
    rust-analyzer
  ];
  bloat = with pkgs; [
    alacritty
    zellij
    xdg-utils
    xdg-user-dirs
    desktop-file-utils
    shared-mime-info
    mpv
    yt-dlp
    feh
    fontpreview
    libreoffice-fresh
    qalculate-gtk
    pavucontrol
    vivaldi
    vivaldi-ffmpeg-codecs
    pcmanfm
    rust-analyzer
    virt-manager
    gimp
    obs-studio
    obs-studio-plugins.input-overlay
    (makeDesktopItem {
      name = "vivaldi-private";
      desktopName = "Private Vivaldi";
      genericName = "Open a private Vivaldi window";
      icon = "vivaldi";
      exec = "${getExe vivaldi} --incognito";
      categories = ["Network"];
    })
  ];
}
