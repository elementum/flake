{pkgs, ...}: let
  waylockWrapper = with pkgs;
    writeShellScriptBin "waylock"
    "${waylock}/bin/waylock $@";
  # "${waylock}/bin/waylock -init-color 0x${base00-hex} -input-color 0x${base04-hex} -fail-color 0x${base0A-hex} $@";
  waylockCommand = "${waylockWrapper}/bin/waylock";
in {
  # user.services.waylock-autolock = {
  #   Unit = {
  #     PartOf = ["graphical-session.target"];
  #     Description = "Lock screen automatically after 15 minutes";
  #     ConditionPathExistsGlob = ["%t/wayland-*"];
  #   };
  #   Service = {
  #     ExecStart = ''
  #       ${pkgs.swayidle}/bin/swayidle \
  #         -w timeout 20 ${waylockCommand} \
  #         before-sleep ${waylockCommand} \
  #         timeout 20 "${pkgs.wlopm}/bin/wlopm --off \*" \
  #         resume "${pkgs.wlopm}/bin/wlopm --on \*" \
  #         lock ${waylockCommand}
  #     '';
  #     Restart = "on-failure";
  #     RestartSec = 1;
  #   };
  #   Install.WantedBy = ["graphical-session.target"];
  # };
  # services = {
  #   seatd = {
  #     enable = true;
  #     description = "Seat management daemon";
  #     script = "${pkgs.seatd}/bin/seatd -g wheel";
  #     serviceConfig = {
  #       Type = "simple";
  #       Restart = "always";
  #       RestartSec = "1";
  #     };
  #     wantedBy = ["multi-user.target"];
  #   };
  # };
}
