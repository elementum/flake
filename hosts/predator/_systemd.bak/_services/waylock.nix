{pkgs, ...}: {
  enable = true;
  description = "Waylock daemon";
  # script = "${pkgs.swayidle}/bin/swayidle -d -w timeout 20 'waylock -fork-on-lock' timeout 20 'hyprctl dispatch dpms off' resume 'hyprctl dispatch dpms on' before-sleep 'waylock -fork-on-lock'";
  serviceConfig = {
    ExecStart = ''
      ${pkgs.swayidle}/bin/swayidle -d -w \
      timeout 20 'waylock -fork-on-lock'
      timeout 20 'hyprctl dispatch dpms off' \
      resume 'hyprctl dispatch dpms on' \
      before-sleep 'waylock -fork-on-lock'
    '';
    # Type = "simple";
    # Restart = "on-failure";
    # RestartSec = "1";
  };
  partOf = ["graphical-session.target"];
  wantedBy = ["hypr-session.target"];
}
