{
  pkgs,
  lib,
  ...
}: {
  swayidle = {
    description = pkgs.swayidle.meta.description;
    documentation = ["man:swayidle(1)"];
    partOf = ["wayland-session.target"];
    wantedBy = ["wayland-session.target"];
    serviceConfig = {
      ExecStart = lib.concatStringsSep " " [
        "${pkgs.swayidle}/bin/swayidle -d -w"
        "timeout 5 'waylock -fork-on-lock'"
        "timeout 5 'hyprctl dispatch dpms off'"
        "resume 'hyprctl dispatch dpms on'"
        "before-sleep 'waylock -fork-on-lock'"
      ];
    };
  };
}
