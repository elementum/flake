{
  wayland-session = {
    description = "Wayland Session";
    bindsTo = ["graphical-session.target"];
    wants = ["graphical-session-pre.target"];
    after = ["graphical-session-pre.target"];
  };
  autostart = {partOf = ["wayland-session.target"];};
}
