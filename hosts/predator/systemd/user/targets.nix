{
  hypr-session = {
    description = "hypr";
    bindsTo = ["graphical-session.target"];
    wants = ["graphical-session-pre.target"];
    after = ["graphical-session-pre.target"];
  };
  autostart = {partOf = ["hypr-session.target"];};
}
