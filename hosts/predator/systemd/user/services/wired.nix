{
  inputs,
  pkgs,
  ...
}: {
  enable = true;
  description = "";
  wantedBy = ["default.target"];
  serviceConfig.Restart = "always";
  serviceConfig.RestartSec = 2;
  serviceConfig.ExecStart = "${inputs.wired.packages.${pkgs.system}.wired}/bin/wired";
  # enable = true;
  # description = "Wired Notify Daemon";
  # documentation = ["https://github.com/Toqozz/wired-notify"];
  # wantedBy = ["default.target"];
  # # wantedBy = ["multi-user.target"];
  # after = ["graphical.target"];
  # serviceConfig = {
  #   ExecStart = "${inputs.wired.packages.${pkgs.system}.wired}/bin/wired";
  #   Restart = "always";
  #   RestartSec = "10";
  #   BusName = "org.freedesktop.Notifications";
  #   Type = "dbus";
  # };
}
