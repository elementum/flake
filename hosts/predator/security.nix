{
  # polkit.enable = true;
  # rtkit.enable = true;
  sudo = {
    enable = false;
    wheelNeedsPassword = false;
  };
  doas = {
    enable = true;
    # TODO TEMPORARY
    wheelNeedsPassword = false;
  };
  pam.services = {
    waylock = {};
  };
  # pam.services.swaylock = {
  #   text = ''
  #     auth include login
  #   '';
  # };
}
