{pkgs}: {
  defaultLocale = "en_US.UTF-8";
  supportedLocales = ["en_US.UTF-8/UTF-8"];
  extraLocaleSettings = {
    LC_TIME = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
  };
  inputMethod = {
    enabled = "fcitx5";
    fcitx5.addons = with pkgs; [
      fcitx5-chinese-addons
      fcitx5-configtool
    ];
  };
}
