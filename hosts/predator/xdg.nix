{
  pkgs,
  inputs,
  ...
}: {
  portal = {
    enable = true;
    wlr.enable = true; # XDG for Wayland
    extraPortals = [
      pkgs.xdg-desktop-portal-gtk
      # inputs.xdg-portal-hyprland.packages.${pkgs.system}.default
    ];
  };
}
