{
  config,
  pkgs,
  ...
}: {
  # cpu.intel.updateMicrocode = true;
  nvidia = {
    # package = nvidiaPkg;
    # package = config.boot.kernelPackages.nvidiaPackages.vulkan_beta;
    # package = config.boot.kernelPackages.nvidiaPackages.beta;
    # package = config.boot.kernelPackages.nvidiaPackages.stable;
    package = config.boot.kernelPackages.nvidiaPackages.latest;
    # powerManagement.enable = true;
    modesetting.enable = true;
    # nvidiaSettings = false;
    powerManagement.enable = true;
  };

  opengl = {
    enable = true;
    driSupport = true;
    driSupport32Bit = true;
    extraPackages32 = with pkgs; [
      libvdpau-va-gl
    ];
    extraPackages = with pkgs; [
      vaapiVdpau
      libvdpau-va-gl
      nvidia-vaapi-driver
    ];
  };
  pulseaudio.support32Bit = true;
}
