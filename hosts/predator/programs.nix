{
  inputs,
  root,
  # pkgs,
  # lib,
  ...
}: let
  hyprland = inputs.hyprland.packages."${root.cfg.system}".default.override {
    enableXWayland = true;
    nvidiaPatches = true;
    hidpiXWayland = false;
  };
in {
  hyprland = {
    enable = true;
    package = hyprland;
  };
  neovim = {
    enable = true;
    withPython3 = true;
    withNodeJs = true;
  };
  git.enable = true;
  nm-applet.enable = true;
  gnupg.agent = {
    enable = true;
    pinentryFlavor = "gnome3";
    enableSSHSupport = true;
  };
  dconf.enable = true;
}
