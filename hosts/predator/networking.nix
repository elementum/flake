{
  firewall.enable = false;
  hostName = "predator";
  useDHCP = false;

  networkmanager = {
    wifi.backend = "iwd";
    enable = true;
  };
  wireless.iwd.enable = true;
}
