{
  pkgs,
  root,
  ...
}:
with root.cfg; {
  time.timeZone = time;

  # hardware.bluetooth.enable = true;

  users.users.${username} = {
    isNormalUser = true;
    shell = pkgs.nushell;
    initialHashedPassword = pass;
    openssh.authorizedKeys.keys = [key];
    extraGroups = [
      "wheel"
      "input"
      "audio"
      "video"
      "seat"
      "storage"
      "docker"
      "libvirtd"
    ];
  };

  system.stateVersion = state;
  powerManagement.cpuFreqGovernor = "performance";
}
