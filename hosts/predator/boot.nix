{pkgs}: {
  loader = {
    grub2-theme = {
      enable = true;
      theme = "tela";
      screen = "4k";
      icon = "color";
      # icon = "whitesur";
    };
    grub = {
      enable = true;
      device = "nodev";
      useOSProber = true;
      efiSupport = true;
    };
    efi.canTouchEfiVariables = true;
    timeout = 2;
  };
  supportedFilesystems = ["btrfs"];

  blacklistedKernelModules = [
    "nouveau"
    # "i2c_hid_acpi"
  ];

  kernelParams = [
    # "quiet"
    # "splash"
    "nvidia-drm.modeset=1"
    # "pcie_aspm.policy=performance"
  ];

  # kernelPackages = pkgs.linuxPackages_latest;
  kernelPackages = pkgs.linuxPackages_latest;
}
