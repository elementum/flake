{
  root,
  src,
}:
with root.cfg; {
  useGlobalPkgs = true;
  useUserPackages = true;
  users.${username} = {
    home.stateVersion = "23.05";
    imports = [
      src.default # import shared home-manager
      # home.desktop
      # inputs.hyprland.homeManagerModules.default
    ];
  };
  extraSpecialArgs = {inherit username;};
}
