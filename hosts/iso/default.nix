{
  config,
  pkgs,
  ...
}: let
  username = "nix";
  ssh-key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDCs1BIuADRyAD3dUP96KaLbMRhnja2+qt5KrwjQwl/Jyn/relfXvhtb0KXXrb7HbXWH3U91FN5PekavGQHIVM6oCO/M4/labKvj29kFLN5lVT6gN56QbaY0L2sD0g5x/WgUbX4eG60PdFIQjIpVOXBBX43VOCGPadCLqF1440j32sJ0K5A83GXqzdIvzSv8Pn57VgOehgtYtBpD3WREgOA+sGOWHKbUvavYa6dNW4LsQQt7hBfmln3NV6NCORvYLu1XKyzTtvQ6RxZ5mhvDklw14h81jD/ktpMXuMITKsbPl/I9OjXUtKQ5NbS0t57taZoQjGqiC05rW64MbQ0ZjayOCaiSEGJOIhkFH5eVs3ZmCoZAESlBUl4bMhyMz7w/aUWQlEfHp4BKXzHUYr7xyqvjIaG+jcTuUKoNGptLlmoJL2Vl52T6nFYHKd4p6eSq/njRGF56yjOczvWm/t3DvBW0+ki0HjzhVKnc4FdqhD/O1+kTt48IHIloNCgVXKKVnOlCzrOubOey14FcLSltv9eJNt46oUhe/cl1ay2RNZAKKFPct3iT7kAVBYpa3YwdPyDOUIniYLRru1vBvIuQzx5+DC9k5lgdKL+ZXMGfMJRj3WOVRKxV8ggUWsbOBP5MyxI9Xo9se3eqzN+HK0a1SMQ+p9d6QURwTuDbaxmgXA9KQ== main";
in {
  # Newest kernels might not be supported by ZFS yet. If you are
  # running an newer kernel which is not yet officially supported by
  # zfs, the zfs module will refuse to evaluate and show up as broken.
  boot.kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
  hardware.enableAllFirmware = true;
  nixpkgs.config.allowUnfree = true;
  environment.systemPackages = with pkgs; [
    helix
    wget
    git
    zfs
    lf
  ];

  # enable nix flakes
  nix = {
    # package = pkgs.nixVersions.stable;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  time = "America/Los_Angeles";
  i18n.defaultLocale = "en_US.UTF-8";

  # find programs
  programs.command-not-found.enable = true;

  services = {
    getty.autologinUser = username;
    openssh.enable = true;
  };

  # enable ssh
  # systemd.services.sshd.wantedBy = pkgs.lib.mkForce ["multi-user.target"];
  users.users.root.openssh.authorizedKeys.keys = [
    ssh-key
  ];
}
