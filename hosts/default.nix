{
  pkgs,
  # lib,
  nixpkgs,
  system,
  src,
  inputs,
  self,
  ...
}: {
  # iso = let
  #   # deadnix: skip
  #   module = {pkgs, ...} @ args:
  #     inputs.haumea.lib.load {
  #       src = ./iso;
  #       inputs =
  #         args
  #         // {
  #           inherit inputs;
  #         };
  #       transformer = inputs.haumea.lib.transformers.liftDefault;
  #     };
  # in
  #   nixpkgs.lib.nixosSystem {
  #     inherit system;
  #     modules = [
  #       module
  #       inputs.home-manager.nixosModules.home-manager
  #       "${nixpkgs}/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix"
  #       {
  #         nix.registry.self.flake = self;
  #         nixpkgs.pkgs = pkgs;
  #       }
  #     ];
  #     specialArgs = {inherit inputs src;};
  #   };
  predator = let
    # deadnix: skip
    module = {pkgs, ...} @ args:
      inputs.haumea.lib.load {
        src = ./predator;
        inputs =
          args
          // {
            inherit inputs;
          };
        transformer = inputs.haumea.lib.transformers.liftDefault;
      };
  in
    nixpkgs.lib.nixosSystem {
      inherit system;
      modules = [
        ./predator/_predator.nix
        module
        inputs.home-manager.nixosModules.home-manager
        inputs.grub2-themes.nixosModules.default
        inputs.hyprland.nixosModules.default
        {
          nix.registry.self.flake = self;
          nixpkgs.pkgs = pkgs;
        }
      ];
      specialArgs = {inherit inputs src;};
    };
}
