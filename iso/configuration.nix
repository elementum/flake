{
  pkgs,
  username,
  modulesPath,
  ...
}: let
  key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDCs1BIuADRyAD3dUP96KaLbMRhnja2+qt5KrwjQwl/Jyn/relfXvhtb0KXXrb7HbXWH3U91FN5PekavGQHIVM6oCO/M4/labKvj29kFLN5lVT6gN56QbaY0L2sD0g5x/WgUbX4eG60PdFIQjIpVOXBBX43VOCGPadCLqF1440j32sJ0K5A83GXqzdIvzSv8Pn57VgOehgtYtBpD3WREgOA+sGOWHKbUvavYa6dNW4LsQQt7hBfmln3NV6NCORvYLu1XKyzTtvQ6RxZ5mhvDklw14h81jD/ktpMXuMITKsbPl/I9OjXUtKQ5NbS0t57taZoQjGqiC05rW64MbQ0ZjayOCaiSEGJOIhkFH5eVs3ZmCoZAESlBUl4bMhyMz7w/aUWQlEfHp4BKXzHUYr7xyqvjIaG+jcTuUKoNGptLlmoJL2Vl52T6nFYHKd4p6eSq/njRGF56yjOczvWm/t3DvBW0+ki0HjzhVKnc4FdqhD/O1+kTt48IHIloNCgVXKKVnOlCzrOubOey14FcLSltv9eJNt46oUhe/cl1ay2RNZAKKFPct3iT7kAVBYpa3YwdPyDOUIniYLRru1vBvIuQzx5+DC9k5lgdKL+ZXMGfMJRj3WOVRKxV8ggUWsbOBP5MyxI9Xo9se3eqzN+HK0a1SMQ+p9d6QURwTuDbaxmgXA9KQ== main";
  pass = "$6$OFaW4HEqtPombFgP$fVK7rjzVVeQGSEkzqmcVsElZcfaO9q66iu.vmBrEUgiZ0ZuUci7NFh0SkI5wP09/b5KPKnad6BEGqZGugPPZp/";
in {
  imports = [
    # (modulesPath + "/installer/cd-dvd/iso-image.nix")
    (modulesPath + "/installer/cd-dvd/installation-cd-minimal.nix")
    (modulesPath + "/profiles/all-hardware.nix")
  ];

  hardware.enableAllFirmware = true;
  time.timeZone = "America/Los_Angeles";
  i18n.defaultLocale = "en_US.UTF-8";

  environment.systemPackages = with pkgs; [
    helix
    wget
    git
    zfs
    lf
    rg
    fd
  ];
  # find programs
  programs = {
    command-not-found.enable = true;
    dconf.enable = true;
  };

  isoImage = {
    makeEfiBootable = true;
    makeUsbBootable = true;
    isoName = "nixos-23.05-${pkgs.stdenv.hostPlatform.system}.iso";
  };

  services = {
    getty.autologinUser = username;
    openssh.enable = true;
  };

  sudo = {
    enable = false;
    wheelNeedsPassword = false;
  };

  doas = {
    enable = true;
    # TODO TEMPORARY
    wheelNeedsPassword = false;
  };

  users.users.root.openssh.authorizedKeys.keys = [
    key
  ];

  users.users.${username} = {
    isNormalUser = true;
    shell = pkgs.nushell;
    initialHashedPassword = pass;
    openssh.authorizedKeys.keys = [key];
    extraGroups = [
      "wheel"
      "input"
      "audio"
      "video"
      "seat"
      "storage"
      "docker"
      "libvirtd"
    ];
  };
}
