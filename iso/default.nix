{
  self,
  username,
  nixpkgs,
  inputs,
  pkgs,
  ...
}: let
  iso = nixpkgs.lib.nixosSystem {
    username = "nix";
    system = "x86_64-linux";
    modules = [
      ./configuration.nix
      inputs.home-manager.nixosModules.home-manager
      {
        home-manager.useGlobalPkgs = true;
        home-manager.useUserPackages = true;
        home-manager.users.${username} = {
          imports = [
            # ./home.nix
            # inputs.hyprland.homeManagerModules.default
          ];
        };
        home-manager.extraSpecialArgs = {inherit username;};
      }
      {
        nix = {
          package = pkgs.nixUnstable;
          extraOptions = ''
            experimental-features = nix-command flakes
          '';
          settings.nix-path = ["nixpkgs=${inputs.nixpkgs}"];
          registry = {
            self.flake = self;
            nixpkgs.flake = inputs.nixpkgs;
          };
        };
        nixpkgs = {
          inherit pkgs;
          config.allowUnfree = true;
        };
        system.stateVersion = "23.05";
      }
    ];
    specialArgs = {inherit inputs username;};
  };
in
  iso.config.system.build.isoImage
