{
  lib,
  root,
}:
with root.colors; let
  inherit (lib) generators;
in
  generators.toINI {} {
    main = {
      terminal = "foot -e";
      font = "${font}:style=Regular:size=18";
      icons-enabled = "yes";
      fuzzy = "yes";
      lines = "15";
      horizontal-pad = "10";
      vertical-pad = "10";
      line-height = "28";
      width = "50";
      dpi-aware = "yes";
      image-size-ratio = "0.5";
      layer = "overlay";
      exit-on-keyboard-focus-loss = "yes";
    };
    colors = {
      selection = "${peach}ff";
      selection-text = "${bg}ff";
      text = "${peach}ff";
      background = "${bg}ff";
      border = "${fg}ff";
    };
  }
