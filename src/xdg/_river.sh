#!/usr/bin/env bash

# shellcheck disable=SC1090
source ~/.config/river/layout

# Fixes super slow loading of gtk apps
riverctl spawn systemctl --user import-environment
# riverctl spawn wlr-randr --output eDP-1 --mode 1920x1080@60 --pos 0,0 --scale 2
riverctl spawn wlr-randr --output eDP-1 --mode 1920x1080@60 --pos 0,0
riverctl spawn "foot --server"
way-displays >/tmp/way-displays."${XDG_VTNR}"."${USER}".log 2>&1 &
# Set repeat rate
riverctl set-repeat 50 300

# Use the "logo" key as the primary modifier
mod="Mod4"

riverctl map normal $mod Q close
riverctl map normal $mod+Shift Q exit
riverctl map normal $mod R spawn "/home/nix/.config/river/init"
riverctl map normal $mod Return spawn footclient
riverctl map normal $mod Space spawn fuzzel
riverctl map normal $mod M spawn pcmanfm
riverctl map normal $mod Z zoom
riverctl map normal $mod F toggle-fullscreen
riverctl map normal $mod O toggle-float
riverctl map-pointer normal Super BTN_MIDDLE toggle-float

# Border color focused
riverctl border-color-focused "0x7da6ff"
# Border color focused
riverctl border-color-unfocused "0x444b6a"

# Super+{Up,Right,Down,Left} to change layout orientation
riverctl map normal $mod Up send-layout-cmd rivertile "main-location top"
riverctl map normal $mod Right send-layout-cmd rivertile "main-location right"
riverctl map normal $mod Down send-layout-cmd rivertile "main-location bottom"
riverctl map normal $mod Left send-layout-cmd rivertile "main-location left"

# Mod+J and Mod+K to focus the next/previous view in the layout stack
riverctl map normal $mod J focus-view next
riverctl map normal $mod K focus-view previous

# Super+Shift+J and Super+Shift+K to swap the focused view with the next/previous
# view in the layout stack
riverctl map normal Super+Shift J swap next
riverctl map normal Super+Shift K swap previous

# Mod+Alt+{H,J,K,L} to move views
riverctl map normal $mod+Mod1 H move left 100
riverctl map normal $mod+Mod1 J move down 100
riverctl map normal $mod+Mod1 K move up 100
riverctl map normal $mod+Mod1 L move right 100

# Super+H and Super+L to decrease/increase the main ratio of rivertile(1)
riverctl map normal Super H send-layout-cmd rivertile "main-ratio -0.05"
riverctl map normal Super L send-layout-cmd rivertile "main-ratio +0.05"

# Super+Shift+H and Super+Shift+L to increment/decrement the main count of rivertile(1)
riverctl map normal Super+Shift H send-layout-cmd rivertile "main-count +1"
riverctl map normal Super+Shift L send-layout-cmd rivertile "main-count -1"

# Mod+Alt+Control+{H,J,K,L} to snap views to screen edges
# riverctl map normal $mod+Shift H snap left
# riverctl map normal $mod+Shift J snap down
# riverctl map normal $mod+Shift K snap up
# riverctl map normal $mod+Shift L snap right

# Mod+Alt+Shif+{H,J,K,L} to resize views
riverctl map normal $mod+Control H resize horizontal -100
riverctl map normal $mod+Control J resize vertical 100
riverctl map normal $mod+Control K resize vertical -100
riverctl map normal $mod+Control L resize horizontal 100

# Mod + Left Mouse Button to move views
riverctl map-pointer normal $mod BTN_LEFT move-view

# Mod + Right Mouse Button to resize views
riverctl map-pointer normal $mod BTN_RIGHT resize-view

for i in $(seq 1 6); do
	tags=$((1 << (i - 1)))

	# Mod+[1-6] to focus tag [0-5]
	riverctl map normal $mod "$i" set-focused-tags $tags

	# Mod+Shift+[1-6] to tag focused view with tag [0-5]
	riverctl map normal $mod+Shift "$i" set-view-tags $tags

	# Mod+Ctrl+[1-6] to toggle focus of tag [0-5]
	riverctl map normal $mod+Control "$i" toggle-focused-tags $tags

	# Mod+Shift+Ctrl+[1-6] to toggle tag [0-5] of focused view
	riverctl map normal $mod+Shift+Control "$i" toggle-view-tags $tags
done

# Mod+0 to focus all tags
# Mod+Shift+0 to tag focused view with all tags
all_tags=$(((1 << 32) - 1))
riverctl map normal $mod 0 set-focused-tags $all_tags
riverctl map normal $mod+Shift 0 set-view-tags $all_tags

# Focused view will stay at top of stack
riverctl attach-mode bottom

# Set app-ids of views which should float
riverctl float-filter-add "pcmanfm"
riverctl float-filter-add "popup"

# Set app-ids of views which should use client side decorations
riverctl csd-filter-add "gedit"
riverctl csd-filter-remove "firefox"

# riverctl spawn rivertile
# riverctl output-layout rivertile
# Set the default layout generator to be rivertile and start it.
# River will send the process group of the init executable SIGTERM on exit.
riverctl default-layout rivertile
rivertile -view-padding 2 -outer-padding 2 &
