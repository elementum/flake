{root}:
with root; {
  "zellij" = {
    source = ./_zellij;
    recursive = true;
  };
  "river/layout" = {
    executable = true;
    source = ./_layout.sh;
  };
  "river/init" = {
    executable = true;
    source = ./_river.sh;
    # text = ''
    # '';
  };
  nushell = {
    source = ./_nu;
    recursive = true;
  };
  eww = {
    source = ./_eww;
    recursive = true;
  };
  helix = {
    source = ./_helix;
    recursive = true;
  };
  "lf/preview" = {
    executable = true;
    source = ./_lf/preview;
  };
  "lf/lfrc".source = ./_lf/lfrc;
  "eww/eww.scss".text = eww;
  "hypr/hyprland.conf".text = hyprland;
  "wofi/style.css".text = wofi;
  "foot/foot.ini".text = foot;
  "fuzzel/fuzzel.ini".text = fuzzel;
  # "user-dirs.defaults".text = ''
  #   DESKTOP=/dev/null
  #   DOCUMENTS=files
  #   DOWNLOAD=files
  #   MUSIC=music
  #   PICTURES=files
  #   PUBLICSHARE=/dev/null
  #   TEMPLATES=/dev/null
  #   VIDEOS=files
  # '';
}
