def send [txt:string] { 
zellij ac move-focus left ; zellij ac write-chars $txt ; zellij ac move-focus right
}
def preview [path:string] { 
if (($path | path type) == 'file') { bat -n --color=always --line-range=:500 $path } else { tree -C $path | head -200 }
}