# settings

let icons = ((
{
"tw": "📁", 
"st": "📁", 
"ow": "📁", 
"dt": "📁", 
"di": "📁", 
"fi": "📄",
"ln": "🔗",
"or": "🔗",
"md": "🔖",
"*.nix": "❄️",
"ex": "📕"
} | 
transpose key value | 
each { |x| $"($x.key)=($x.value)"}
) | str join ':')

let colors = ((
{
"~/Documents": "01;31",
"~/Downloads": "01;31",
"~/Pictures": "01;31",
"README.*": "33",
"*.txt": "34",
"*.md": "34",
"ln": "01;36",
"di": "01;34",
"ex": "01;32"
} | 
transpose key value | 
each { |x| $"($x.key)=($x.value)"}
) | str join ':')


{
PATH: [
/usr/local/sbin
/usr/sbin
/sbin
/usr/local/bin
/usr/bin
/run/wrappers/bin
/home/nix/.nix-profile/bin
/etc/profiles/per-user/nix/bin
/nix/var/nix/profiles/default/bin
/run/current-system/sw/bin
$"($env.HOME)/.local/bin"
$"($env.HOME)/.nix-profile/bin"
$"($env.HOME)/.fzf/bin"
$"($env.HOME)/.pyenv/bin"
# $"($env.HOME)/bin"
$"($env.HOME)/go/bin"
$"($env.HOME)/.gnupg"
$"($env.HOME)/.volta/bin"
$"($env.HOME)/.cargo/bin"
]
} | load-env

# MANPAGER: 'nvim +Man!'
# PAGER: 'bat'

{
BROWSER: "vivaldi"
LF_ICONS: $icons
LF_COLORS: $colors
LS_COLORS: (vivid generate ayu)
PAGER: 'less -FRSX'
MANPAGER: "sh -c 'col -bx | bat --language man --plain'"
EDITOR: "hx"
VISUAL: "hx"
FZF_FIND_FILE: "fd --hidden --exclude .git --strip-cwd-prefix --type f"
RG_PREFIX: "rg --column --line-number --no-heading --color=always --smart-case -g '!.git'"
FZF_DEFAULT_OPTS: ([
"--info inline"
"--extended"
"--ansi"
"--multi"
"--prompt '∷ '"
"--pointer '▶'"
"--marker '┃'"
"--color=fg:-1,bg:-1,hl:#ffaf5f,fg+:-1,bg+:-1,hl+:#ffaf5f"
"--color=prompt:#5fff87,marker:#ff87d7,spinner:#ff87d7"
"--bind ctrl-u:preview-up,ctrl-d:preview-down"
"--bind ctrl-w:half-page-up,ctrl-s:half-page-down"
"--bind ctrl-a:select-all,ctrl-r:toggle-all,ctrl-l:deselect-all"
"--bind ctrl-t:toggle-sort"
"--bind ?:toggle-preview"
"--preview 'bat --theme=TwoDark --wrap=auto --style=numbers,header,changes,snip --color=always {1}'"
"--preview-window up,40%,border-bottom,+{2}+3/3,~3"
] | str join " ")
FZD: "bash ~/m/hax/bash/fzf/fd.sh"
FZR: "bash ~/m/hax/bash/fzf/fr.sh"
FLAKE: "bash ~/m/hax/bash/rsync/flake.sh"
DOT: $"($env.HOME)/.dot"
NNN_PLUG: "r:rg;f:open;j:jump;p:preview-tui;d:dif"
NNN_FIFO: "/tmp/nnn.fifo"
NIXPKGS_ALLOW_UNFREE: "1"
} | load-env

# starship
{
PROMPT_COMMAND: { ||
  let width = (term size).columns
  ^starship prompt $"--cmd-duration=($env.CMD_DURATION_MS)" $"--status=($env.LAST_EXIT_CODE)" $"--terminal-width=($width)"
}
STARSHIP_SHELL: "nu"
STARSHIP_SESSION_KEY: (random chars -l 16)
PROMPT_INDICATOR: ""
PROMPT_INDICATOR_VI_INSERT: "🐚 "
PROMPT_INDICATOR_VI_NORMAL: "📕 "
PROMPT_MULTILINE_INDICATOR: (^starship prompt --continuation)
} | load-env
# if not ("~/.cache/starship" | path exists) { mkdir "~/.cache/starship" }
# if not ("~/.cache/starship/init.nu" | path exists) { starship init nu | save ~/.cache/starship/init.nu }
