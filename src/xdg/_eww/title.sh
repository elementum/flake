#!/bin/bash
hyprctl activewindow -j | jq --raw-output .title | cut -c -63
# shellcheck disable=SC2016
socat -u UNIX-CONNECT:/tmp/hypr/"$HYPRLAND_INSTANCE_SIGNATURE"/.socket2.sock - | stdbuf -o0 grep '^activewindow>>' | stdbuf -o0 awk -F '>>|,' '{print $3}'
