{root, ...}:
with root.colors.gtk; {
  inherit enable theme iconTheme gtk3 gtk4;
}
