{root}:
with root.colors; ''
  $blue: #${blue};
  $red: #${red};
  $bg: #${bg};
  $fg: #${fg};
  $magenta: #${magenta};
  $yellow: #${yellow};
  $green: #${green};

  * {
    all: unset; //Unsets everything so you can style everything from scratch
    // font,icon size
    font: 18px "${font}";
  }

  .gray {
    color: $fg;
    background-color: $bg;
  }

  .red {
    color: $red;
    background-color: $bg;
  }

  .green {
    color: $green;
    background-color: $bg;
  }

  .blue {
    color: $blue;
    background-color: $bg;
  }

  .yellow {
    color: $yellow;
    background-color: $bg;
  }

  .magenta {
    color: $magenta;
    background-color: $bg;
  }

  .cpubar {
    color: $blue;
    background-color: $bg;
    margin: 2px 0;
  }

  .diskbar {
    color: $green;
    background-color: $bg;
    margin: 2px 0;
  }

  .membar {
    color: $yellow;
    background-color: $bg;
    margin: 2px 0;
  }

  // bar background
  .bar {
    background-color: $bg;
  }
''
