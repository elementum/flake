{
  pkgs,
  root,
  ...
}: {
  swayidle = with pkgs; let
    waylockWrapper = with root.colors;
      writeShellScriptBin "waylock"
      "${waylock}/bin/waylock -init-color 0x${black} -input-color 0x${bg} -fail-color 0x${darkpurple} $@";
    lock = "${waylockWrapper}/bin/waylock -fork-on-lock";
    dpmsOn = "${hyprland}/bin/hyprctl dispatch dpms on";
    dpmsOff = "${hyprland}/bin/hyprctl dispatch dpms off";
    time = 300;
  in {
    enable = true;
    events = [
      {
        event = "after-resume";
        command = dpmsOn;
      }
      {
        event = "before-sleep";
        command = lock;
      }
      {
        event = "lock";
        command = lock;
      }
    ];
    timeouts = [
      {
        timeout = time;
        command = lock;
      }
      {
        timeout = time;
        command = dpmsOff;
        resumeCommand = dpmsOn;
      }
    ];
    systemdTarget = "hypr-session.target";
  };
}
