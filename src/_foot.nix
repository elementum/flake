{
  lib,
  root,
}:
with root.colors; let
  inherit (lib) generators;
in
  generators.toINI {} {
    main = {
      term = "xterm-256color";
      font = "${font}:size=14";
      pad = "24x24";
      dpi-aware = "yes";
    };
    colors = {
      alpha = "1";
      foreground = "${fg}";
      background = "${bg}";
    };
    mouse = {
      hide-when-typing = "yes";
    };
  }
