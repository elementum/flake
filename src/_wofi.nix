{root}:
with root.colors; ''
  * {
    font: 18px "${font}";
  }
  window {
  	font-family: "${font}";
  	background: transparent;
  }

  #outer-box {
  	padding: 10px;
  	border-radius: 8px;
  	background: #${black};
  }

  #scroll {
  	/* The Nordic gtk theme adds an outline to show scroll areas... */
  	outline-color: transparent;
  }

  #input {
  	color: #${peach};
  	caret-color: #${peach};
  	background: #${black};
  	border-top-color: #${black};
  	border-left-color: #${black};
  	border-right-color: #${black};
  	border-bottom-color: #${black};
  	box-shadow: 0 0 0 1px transparent inset;
  	outline-color: transparent;
  }

  #input:focus {
  	background: #${black};
  	border-color: #${black};
  	box-shadow: 0 0 0 1px transparent inset;
  	border-top-color: #${black};
  	border-left-color: #${black};
  	border-right-color: #${black};
  	border-bottom-color: #${black};
  	box-shadow: none;
  	outline-color: transparent;
  }

  #input image.left {
  	color: #d8dee9;
  }

  #input:focus image.left {
  	color: #${peach};
  }

  #input image.right {
  	color: #d8dee9;
  }

  #input:focus image.right {
  	color: #${peach};
  }

  label {
  	/* We set backgrounds on the block level. */
  	background: transparent;
  }

  #scroll {
  	padding-top: 6px;
  }

  #entry {
  	color: #${peach};
  	padding: 8px 8px;
  	border-radius: 4px;
  	background: transparent;
  }

  #entry:selected {
  	color: #${white};
  	background: #${gray};
  	font-weight: bold;
  }

  expander arrow {
  	margin-right: 8px;
  }

  #entry #selected #text {
  	color: #${white};
  }

  expander list {
  	margin-top: 8px;
  	/* background: #8fbcbb; */
  	background: transparent;
  	padding-left: 16px;
  }

  expander list #entry {
  	transition: none;
  	background: transparent;
  }

  expander list #entry:hover,
  expander list #entry:active {
  	/* color: #8fbcbb;
  	background: #${peach}; */
  }

  expander list #entry #selected {
  	background: #8fbcbb;
  }

  expander list #entry #selected label {
  	color: #${white};
  	font-weight: bold;
  }

  expander list #entry:hover,
  expander list #entry:active {
  	background: #8fbcbb;
  }

  expander list #entry:hover label,
  expander list #entry:active label {
  	color: #${white};
  	font-weight: bold;
  }

  expander list label {
  	color: #d8dee9;
  }
''
