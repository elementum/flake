{root}:
with root.colors; ''
  monitor=${host.monitor},1920x1080@60,0x0,1.3

  # exec
  exec-once=foot --server
  exec-once=eww daemon
  exec-once=eww open bar
  exec-once=hyprpaper
  # exec-once=wired &
  exec-once=systemctl --user start hypr-session.target

  input {
      kb_layout = us
      follow_mouse = 1
      touchpad {
          natural_scroll = no
      }
      sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
      repeat_rate=50
      repeat_delay=240
  }

  general {
      gaps_in = 0
      gaps_out = 0
      border_size = 3
      col.active_border = rgb(${red})
      col.inactive_border = rgb(${gray})
      # fix cursor?
      no_cursor_warps=true
      layout = dwindle
  }
  decoration {
    multisample_edges = true
    active_opacity = 1.0
    inactive_opacity = 1.0
    fullscreen_opacity = 1.0
    rounding = 0
    blur = yes
    blur_size = 10
    blur_passes = 1
    blur_new_optimizations = true
    blur_xray = true
    drop_shadow = false
    shadow_range = 4
    shadow_render_power = 3
    shadow_ignore_window = true
    dim_inactive = false
    blur_ignore_opacity = false
    col.shadow = rgba(1a1a1aee)
  }

  dwindle {
    no_gaps_when_only = false
    force_split = 0
    special_scale_factor = 0.8
    split_width_multiplier = 1.0
    use_active_for_splits = true
    pseudotile = yes
    preserve_split = yes
  }

  misc {
    disable_hyprland_logo = true
    focus_on_activate = true
  }

  master {
    new_is_master = true
    special_scale_factor = 0.8
    new_is_master = true
    no_gaps_when_only = false
  }

  gestures {
      workspace_swipe = off
  }
  device:epic mouse V1 {
      sensitivity = -0.5
  }
  windowrulev2 = float, title:^(Firefox — Sharing Indicator|Picture-in-Picture|About Mozilla Firefox|Open File|Open Folder|Select one or more files to open|File Upload)$
  windowrulev2 = float, class:^(pavucontrol|udiskie|kitty-float)$
  # Enable background blur on all windows :)
  # Disable animations for the slurp selection layer so it doesn't end up in the screenshot
  layerrule = noanim, selection

  $mod = SUPER

  bind = $mod, Return, exec, footclient
  bind = $mod, Q, killactive,
  bind = $mod SHIFT, Q, exit
  bind = $mod, M, exec, pcmanfm
  bind = $mod, O, togglefloating,
  bind = $mod, F, fullscreen, 1
  bind = $mod, Space, exec, fuzzel

  bind = $mod, mouse_down, workspace, e+1
  bind = $mod, mouse_up, workspace, e-1

  bindm = $mod, mouse:272, movewindow
  bindm = $mod, mouse:273, resizewindow

  bind = $mod, P, pseudo, # dwindle
  bind = $mod, S, togglesplit, # dwindle
  bind = $mod, W, togglesplit, # dwindle

  bind = $mod, h, movefocus, l
  bind = $mod, l, movefocus, r
  bind = $mod, j, movefocus, d
  bind = $mod, k, movefocus, u

  bind = $mod CTRL, h, resizeactive, -20 0
  bind = $mod CTRL, l, resizeactive, 20 0
  bind = $mod CTRL, j, resizeactive, 0 20
  bind = $mod CTRL, k, resizeactive, 0 -20

  bind = $mod SHIFT, h, movewindow, l
  bind = $mod SHIFT, l, movewindow, r
  bind = $mod SHIFT, j, movewindow, d
  bind = $mod SHIFT, k, movewindow, u

  bind = $mod, 1, workspace, 1
  bind = $mod, 2, workspace, 2
  bind = $mod, 3, workspace, 3
  bind = $mod, 4, workspace, 4
  bind = $mod, 5, workspace, 5

  bind = $mod SHIFT, 1, movetoworkspace, 1
  bind = $mod SHIFT, 2, movetoworkspace, 2
  bind = $mod SHIFT, 3, movetoworkspace, 3
  bind = $mod SHIFT, 4, movetoworkspace, 4
  bind = $mod SHIFT, 5, movetoworkspace, 5
''
