# modified from https://github.com/joshdick/onedark.vim
{pkgs, ...}: let
  theme = "Breeze-Dark";
  font = "Sarasa Mono SC";
in {
  inherit theme font;
  host = {
    monitor = "eDP-1";
  };
  # black = "15161e";
  black = "11100e";
  blue = "61afef";
  cyan = "56b6c2";
  dimgray = "282c34";
  gray = "2c323c";
  green = "98c379";
  lightgray = "5c6370";
  lightred = "e06c75";
  magenta = "c678dd";
  orange = "d19a66";
  red = "d7005f";
  silver = "4b5263";
  white = "abb2bf";
  yellow = "e5c07b";
  peach = "cc9393";
  bg = "1d1d1d";
  fg = "8a8a8a";
  purple = "45313c";
  darkpurple = "400224";
  icon = {
    name = "Arc";
    package =
      pkgs.arc-icon-theme;
    # pkgs.papirus-icon-theme;
  };
  pointer = {
    name = "Bibata-Modern-Classic";
    package = pkgs.bibata-cursors;
    size = 12;
  };
  gtk = let
    gtkSettings = {
      gtk-theme-name = theme;
      gtk-font-name = "${font} 30";
      gtk-cursor-blink = false;
      gtk-recent-files-limit = 20;
      gtk-application-prefer-dark-theme = 1;
      gtk-toolbar-style = "GTK_TOOLBAR_BOTH";
      gtk-toolbar-icon-size = "GTK_ICON_SIZE_LARGE_TOOLBAR";
      gtk-button-images = 1;
      gtk-menu-images = 1;
      gtk-enable-event-sounds = 1;
      gtk-enable-input-feedback-sounds = 1;
      gtk-xft-antialias = 1;
      gtk-xft-hinting = 1;
      gtk-xft-hintstyle = "hintfull";
      gtk-xft-rgba = "rgb";
    };
  in {
    enable = true;
    theme = {
      package = pkgs.breeze-gtk;
      name = theme;
    };
    iconTheme = {
      name = "Arc";
      package =
        pkgs.arc-icon-theme;
      # pkgs.papirus-icon-theme;
    };
    gtk3.extraConfig = gtkSettings;
    gtk4.extraConfig = gtkSettings;
  };
}
